<?php
/**
 * @file
 * The smart_redirect module API extend.
 */

/**
 * Implements hook_analyzer_types().
 */
function hook_analyzer_types() {
  // Key - machine_name of analyzer.
  // Value - callback/function.
  $types['levenshtein'] = 'smart_redirect_levenshtein_analyzer';

  return $types;
}

/**
 * Levenshtein analyzer example.
 */
function smart_redirect_levenshtein_analyzer($source) {
  $alias = array();

  $query = db_select('url_alias', 'u')
    ->fields('u', array('pid', 'source', 'alias'));
  $result = $query->execute();

  while ($record = $result->fetchAssoc()) {
    $length = levenshtein($source, $record['alias']);
    $length = number_format($length, 2);
    if ($length) {
      $similar_results[$length] = array(
        'source' => $record['source'],
        'alias' => $record['alias'],
      );
    }
  }
  if (!empty($similar_results)) {
    ksort($similar_results);
    $alias = $similar_results[key($similar_results)]['alias'];
  }

  return $alias;
}
